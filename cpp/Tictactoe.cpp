#include "Tictactoe.hpp"

Jeu::Jeu() {
    raz();
}

std::array<std::array<Joueur,3>,3> Jeu::getPlateau()const{
	return _plateau;
}

void Jeu::raz() {
    for (auto & l : _plateau)
    {
		for (auto & c : l){
				c=JOUEUR_VIDE;
		}
	}
}

std::ostream & operator<<(std::ostream & os, const Jeu & jeu) {
    for (auto & l : jeu.getPlateau())
    {
		for (auto & c : l)
		{
			if(c == JOUEUR_ROUGE)os<< 'R';
			else if (c == JOUEUR_VERT)os<<'V';
			else os<<'.';
		}
		os << std::endl;
	}
    return os;
}



Joueur Jeu::getVainqueur() const {
    // TODO
    return JOUEUR_VIDE;
}

Joueur Jeu::getJoueurCourant() const {
    // TODO
    return JOUEUR_VIDE;
}

bool Jeu::jouer(int i, int j) {
    // TODO
    return false;
}

